package com.upjv.siftproject.config;

import com.upjv.siftproject.imgproc.FeatureDescriptionImage;
import com.upjv.siftproject.imgproc.FeatureExtractionImage;
import java.io.File;

//import imgproc.FeatureDescriptionImage;
//import imgproc.FeatureExtractionImage;

public class ImageData {
	public static File img1, img2;
	public static FeatureExtractionImage feaExtImg1, feaExtImg2;
	public static FeatureDescriptionImage feaDesImg;
}
