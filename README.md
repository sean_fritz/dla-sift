**A lire pour comprendre le fonctionnement et l'organisation du repo**

Le dépot est organisé en dossier comme il suit :

* **images** :  contenant des exemples d'images
* **dll** : contenant le fichier .dll de opencsv nécessaire au fonctionnement de l'application
* **SiftProject** : les codes source du projet (maven)
* **jar** : contenant un jar directement utilisable du projet en ligne de commande

*Tout au long des différentes procédures d'exploitation, il sera nécessaire de toujours au préalable s'assurer que le path du fichier dll est bien configuré faute quoi l'application ne marchera pas*

---

## Téléchargement du projet via git

Pour cloner le projet en local chez vous, procéder comme il suit:

1. git clone https://sean_fritz@bitbucket.org/sean_fritz/dla-sift.git

---

## Téléchargement du projet en zip

Pour télécharger le projet en entier procéder comme il suit:

1. Dans le de gauche clicker sur le bouton **Downloads**
2. Dans la nouvelle fenêtre, lancer le téléchargement du zip contenant les sources via le bouton **Download repository **


---

## Exécution du jar et utilisation de l'application

L'exécution du jar demande d'avoir comme prérequis java installé.

1. Ouvrir un terminal
2. Se positionner dans le dossier racine du projet
3. Exécuter la commande : java -Djava.library.path=dll -jar jar/SiftProject.jar
4. Dans la fénêtre de l'application choisir deux images via les boutons **Load Image 1** et **Load Image 2** 
5. Cliquer sur **1 Extract feature** pour extraire les points saillants puis **2. Describe Feature** pour les mettres en correspondance
